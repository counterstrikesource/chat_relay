FROM ubuntu:20.04

COPY . /app
WORKDIR /app

RUN apt update && \
    apt upgrade -y && \
    apt install -y ca-certificates

RUN chmod +x ./relay_bot

CMD ./relay_bot
