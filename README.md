# Chat Relay

Chat relay for srcds servers to Discord

## Docker compose

```
version: '3.7'

services:
  portainer:
    image: registry.gitlab.com/counterstrikesource/chat_relay
    container_name: chat_relay_css_zr
    restart: always
    ports:
      - 33337:33337
    volumes:
      - ./config.toml:/app/config.toml
```
